// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { Alert, Image, StyleSheet } from 'react-native';
// import Home from "../Screens/Home"

// import { useDispatch, useSelector } from 'react-redux';

// const Tab = createBottomTabNavigator();
// const BottomTab = () => {
//     // const dispatch = useDispatch();
//     return (
//         <Tab.Navigator tabBarOptions={{ activeTintColor: "black" }}>
//             <Tab.Screen

//                 name="Browse"
//                 component={Home}

               
//             />

//             <Tab.Screen
//                 name="Favorities"
//                 component={Home}
                
//             />
//             <Tab.Screen
//                 name="Profile"
//                 component={Home}
               
//             />
//         </Tab.Navigator>
//     );
// };

// export default BottomTab;
// const styles = StyleSheet.create({

//     tabImg2: {
//         alignSelf: 'center',
//         tintColor: 'grey',
//         height: 25,
//         width: 30,
//     },
//     selectedTabImg2: {
//         alignSelf: 'center',
//         tintColor: 'black',
//         height: 25,
//         width: 30,
//     },

// });




import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet } from 'react-native';
import Home from "../Screens/Home"
const Tab = createBottomTabNavigator();
const BottomTab = () => {
    return (
        <Tab.Navigator tabBarOptions={{ activeTintColor: "#0aa116", labelStyle: { fontSize: 10, width: 100, fontWeight: "600" } }}>
            
            <Tab.Screen

                name="Home"
                component={Home}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        // <Icon
                        //     name='file-text-o'
                        //     size={17}
                        //     color={'#0aa116'}

                        // />
                        <Image source={require("../Assets/logo.png")} style={{ height: 20, width: 20, }} resizeMode={"contain"}></Image>

                    ),
                })}
            />
        </Tab.Navigator>
    );
};

export default BottomTab;
const styles = StyleSheet.create({

    tabImg2: {
        alignSelf: 'center',
        tintColor: 'grey',
        height: 25,
        width: 30,
        backgroundColor: "red"
    },
    selectedTabImg2: {
        alignSelf: 'center',
        tintColor: 'black',
        height: 25,
        width: 30,
        backgroundColor: "red"

    },

});