import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../Screens/AuthScreens/Splash';
import Home from "../Screens/Home"
import BottomTabScreen from "../Screens/BootmTabScreen"
const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
       
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
     <Stack.Screen
        name="BottomTabScreen"
        component={BottomTabScreen}
        options={{headerShown: false}}
      />
      
    </Stack.Navigator>
  );
};

export default MainStackNavigator;
