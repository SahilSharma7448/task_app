import React, {useEffect} from 'react';
import {Text, View, Dimensions, Image} from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
const {strings, colors, fonts, urls, PATH} = constants;
import * as Utility from "../../../Utility/index"
const Splash = ({navigation}) => {
  useEffect(() => {
    // timeoutHandle = setTimeout(() => {
      retrieveData();
  }, []);
  const retrieveData = async() => {
   navigation.navigate('BottomTabScreen')
  };
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      
    </View>
  );
};

export default Splash;
