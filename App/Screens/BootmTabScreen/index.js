import React, {useEffect,useState} from 'react';
import {Text, View, Dimensions, Image} from 'react-native';
import constants from '../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../Utility/index';
const {strings, colors, fonts, urls, PATH} = constants;
import * as Utility from "../../Utility/index"
import Home from "../Home"
import Profile from "../Profile"
const BottomTabScreen = ({navigation}) => {
const [selectedTab,setSelectedTab]=useState('home')
  return (
    <View
      style={{
        flex: 1,
        
      }}>
{selectedTab=='home'?
<Home></Home>:<Profile></Profile>}
        <View style={{bottom:0,position:"absolute",padding:hp('2%'),flexDirection:"row",}}>
      <Text style={{fontSize:25,width:wp('70%'),color:selectedTab=='home'?'red':'black'}} onPress={()=>setSelectedTab('home')}>Home</Text>
      <Text style={{fontSize:25,color:selectedTab=='profile'?'red':'black'}} onPress={()=>setSelectedTab('profile')}>Profile</Text>

      </View>
    </View>
  );
};

export default  BottomTabScreen;
    
