import {
  TimeValue,

} from './Const';
const initialState = {
  TimeValue: 0,

};

////========== SET ALL DATA IN STORE BY UNIQUE STATE ========
export default function (state = initialState, {type, payload}) {
  switch (type) {
    case TimeValue: {
      return {
        ...state,
        TimeValue: payload,
      };
    }
   
    default:
      return state;
  }
}
