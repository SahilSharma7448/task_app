const colors = {
  title_Colors: '#9D0932',
  white_Colors: 'white',
  grey_Background: '#E8E8E8',
  grey_Text: 'grey',
  black_Text: 'black',
  trasparent_Color: '#fff0',
  textHeading_Color: '#383E56',
  light_Grey: '#bfbfbf',
};

export default colors;
