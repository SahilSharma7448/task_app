import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Button} from 'react-native-elements';
import constants from '../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import OptionsMenu from 'react-native-option-menu';

const {strings, colors, fonts, urls, PATH} = constants;
const Header = ({
  navigation,
  title,
  click,
  showPlus,
  route,
  showDot,
  dotPlus,
}) => {
  return (
    <View>
      <View
        style={{
          ...Platform.select({
            ios: {
              marginTop: hp('5%'),
            },
            android: {
              marginTop: hp('3%'),
            },
          }),
          padding: wp('5%'),
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <View
              style={{
                height: hp('3%'),
                width: wp('8%'),
                borderColor: constants.title_Colors,
                borderWidth: 1,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon
                name={'long-arrow-left'}
                size={18}
                color={constants.title_Colors}
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </TouchableOpacity>
          <Text
            style={{marginLeft: wp('3%'), fontWeight: 'bold', fontSize: 17}}>
            {title}
          </Text>
        </View>
        {showPlus == true ? (
          <TouchableOpacity onPress={() => navigation.navigate(route)}>
            <Icon
              name={'plus-circle'}
              size={25}
              color={constants.title_Colors}
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </TouchableOpacity>
        ) : null}
        {showDot == true ? (
          <OptionsMenu
            customButton={
              <Entypo
                name={'dots-three-vertical'}
                size={20}
                color={constants.title_Colors}
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            }
            destructiveIndex={1}
            options={['Edit', 'Delete', 'Cancel']}
            // actions={[editPost, deletePost]}
          />
        ) : null}
      </View>
    </View>
  );
};

export default Header;
